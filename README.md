# Measuring NC Implementation Characteristics

 - external-latency.rkt is the driver for the roundtrip latency "echo"
   programs. Generally speaking, statistics for each run will be
   recorded into "client-grow-times", "client-ping-times" and
   "client-summary" tables. When the server for a run is written in
   Racket, additional statistics will be collected into tables
   "server-memory-use" and "server-connections".
    - echo-client.rkt measures roundtrip latency of pinging a remote server
    - echoserver.erl is a matching ping server in Erlang
    - echo-server-minimart-tcp-driver.rkt is a server using Minimart's TCP driver
    - echo-server-prospect-tcp-driver.rkt is a server using Prospect's TCP driver
    - echo-server-imperative-syndicate-tcp-driver.rkt is a server using Imperative Syndicate's TCP driver
    - echo-server.rkt is a server using Minimart, but eschewing the TCP driver
    - plain-racket-server.rkt is a server using built-in Racket threads
    - uvserver.c is a server written in C with libuv

 - internal-latency.rkt measures roundtrip latency of pinging a local
   peer in a Minimart world.
    - internal-latency-symbols-not-fixnums.rkt is the same, but uses
      symbols instead of fixnums to identify peers.
    - internal-latency-prospect.rkt is the same, but using Prospect
      instead of Minimart.
    - internal-latency-imperative-syndicate.rkt is the same, but using
      Imperative Syndicate instead of Minimart.

 - observe-all-minimart.rkt, observe-all-prospect.rkt,
   observe-all-imperative-syndicate.rkt, sum-all-minimart.rkt,
   sum-all-prospect.rkt, and sum-all-imperative-syndicate.rkt measure
   the costs of presence notification and processing in Minimart,
   Prospect, and Imperative Syndicate worlds, respectively.

 - pingpong.rkt and pingpong.erl are simple measurements of Racket's
   and Erlang's built-in thread communication latency, respectively.
   They do not use the racket-logger statistics-collection subsystem,
   instead printing their results to stdout.
