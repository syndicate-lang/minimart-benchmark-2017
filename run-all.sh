#!/bin/sh
MAX_WAYPOINT=25000
MAX_MINIMART_WAYPOINT=200
# MAX_WAYPOINT=1100
# MAX_MINIMART_WAYPOINT=100

set -e
make

set +e

racket external-latency.rkt --max-waypoint $MAX_WAYPOINT --erlang
racket external-latency.rkt --max-waypoint $MAX_WAYPOINT --uv
## racket external-latency.rkt --max-waypoint $MAX_MINIMART_WAYPOINT --minimart ## don't bother
racket external-latency.rkt --max-waypoint $MAX_MINIMART_WAYPOINT --minimart+tcp
racket external-latency.rkt --max-waypoint $MAX_WAYPOINT --prospect+tcp
racket external-latency.rkt --max-waypoint $MAX_WAYPOINT --imperative-syndicate+tcp
racket external-latency.rkt --max-waypoint $MAX_WAYPOINT --racket
racket external-latency.rkt --max-waypoint $MAX_WAYPOINT --racket-evt

racket internal-latency.rkt
racket internal-latency-prospect.rkt
racket internal-latency-imperative-syndicate.rkt

racket broadcast-latency-prospect.rkt
racket broadcast-latency-imperative-syndicate.rkt

racket observe-all-minimart.rkt
racket observe-all-prospect.rkt
racket observe-all-imperative-syndicate.rkt

racket observe-some-minimart.rkt
racket observe-some-prospect.rkt
racket observe-some-imperative-syndicate.rkt
