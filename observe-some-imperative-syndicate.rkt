#lang imperative-syndicate/test-implementation
;; Measurement of presence processing.
;; Peers observe just one distinguished peer, and do not process resulting routing events.

(require logbook)

(provide run)

(assertion-struct client ())

(define (run #:peer-count [peer-count 100] #:client-first? [client-first? #f])
  (define event-count 0)

  (define-values (results cpu-total-ms wall-ms cpu-gc-ms)
    (time-apply
     (lambda ()
       (test-case
           [(define (spawn-client)
              (spawn (assert (client))))

            (define (spawn-server id)
              (spawn (on (asserted (client))
                         (set! event-count (+ event-count 1)))))

            (define (spawn-servers)
              (for [(id (in-range (- peer-count 1)))]
                (spawn-server id)))

            (if client-first?
                (begin (spawn-client) (spawn-servers))
                (begin (spawn-servers) (spawn-client)))]))
     '()))

  (values event-count (max 1 (- cpu-total-ms cpu-gc-ms))))

(module+ main
  (define E (standard-logbook-entry (default-logbook #:verbose? #t) "minimart" "observe-some-imperative-syndicate"))
  (define T-client-first (logbook-table E "presence-processing-narrow-client-first"
                                        #:column-spec '(number-of-peers
                                                        secs/routing-update
                                                        routing-updates/sec
                                                        secs/peer
                                                        peers/sec
                                                        event-count
                                                        run-duration-ms)))
  (define T-client-last (logbook-table E "presence-processing-narrow-client-last"
                                       #:column-spec '(number-of-peers
                                                       secs/routing-update
                                                       routing-updates/sec
                                                       secs/peer
                                                       peers/sec
                                                       event-count
                                                       run-duration-ms)))
  ;; Warmup
  (let ()
    (run #:peer-count 1)
    (run #:peer-count 10)
    (void))
  ;; Real run
  (for ((n
         #;(list 5)
         (list* 2 5
                (let loop ((n 10))
                  (if (>= n 100000)
                      '()
                      (cons (inexact->exact (round n))
                            (loop (* n (sqrt (sqrt 2))))))))
	 ))
    (collect-garbage)
    (collect-garbage)
    (collect-garbage)
    (for ((client-first? (list #t #f)))
      (define-values (event-count run-duration-ms)
        (run #:peer-count n #:client-first? client-first?))
      (write-logbook-datum! (if client-first? T-client-first T-client-last)
                            (list n
                                  (/ (/ run-duration-ms 1000.0) event-count)
                                  (/ event-count (/ run-duration-ms 1000.0))
                                  (/ (/ run-duration-ms 1000.0) n)
                                  (/ n (/ run-duration-ms 1000.0))
                                  event-count
                                  run-duration-ms)))))
