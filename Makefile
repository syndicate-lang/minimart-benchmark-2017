all: uvserver
	raco make *.rkt

clean:
	rm -rf compiled
	rm -f uvserver

uvserver: uvserver.c
	gcc -o $@ $< -luv
