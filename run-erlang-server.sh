#!/bin/sh
erlc echoserver.erl
erl \
    +K true \
    +A30 \
    +P 1048576 \
    -kernel inet_default_connect_options '[{nodelay,true}]' \
    -noshell \
    -run echoserver start
